package es.franam.microserviciopeliculasactoresback.controladores;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


import es.franam.microserviciopeliculasactoresback.entitades.Pelicula;
import es.franam.microserviciopeliculasactoresback.servicios.IPeliculaService;

@RestController
public class PeliculaRestController {

	@Autowired
	private IPeliculaService peliculaService;

	
	@GetMapping("/peliculas")
	public List<Pelicula> listar() {
		return peliculaService.buscarTodas();
	}

	@GetMapping("/peliculas/{id}")
	public Pelicula buscarPorId(@PathVariable Long id) {
		return peliculaService.buscarPorId(id);
	}

	@PostMapping("/peliculas")
	public Pelicula guardar(@RequestBody Pelicula pelicula) {
		return peliculaService.guardar(pelicula);
	}

	@PutMapping("/peliculas")
	public Pelicula actualizar(@RequestBody Pelicula pelicula) {
		return peliculaService.actualizar(pelicula);
		
	}

	@DeleteMapping("/peliculas/{id}")
	public void eliminar(@PathVariable Long id) {
		peliculaService.eliminar(id);
	}
	

	@GetMapping("/peliculas/titulo/{titulo}")
	public List<Pelicula> buscarPorTitulo(@PathVariable String titulo) {
		return peliculaService.buscarPorGenero(titulo);
	}

	@GetMapping("/peliculas/genero/{genero}")
	public List<Pelicula> buscarPorGenero(@PathVariable String genero) {
		return peliculaService.buscarPorGenero(genero);
	}

	@GetMapping("/peliculas/actor/{nombre}")
	public List<Pelicula> buscarPorActor(@PathVariable String nombre) {
		return peliculaService.buscarPorActor(nombre);
	}

	

}
